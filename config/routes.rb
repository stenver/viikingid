Vikings::Application.routes.draw do

  get "/culture", to: "static_pages#culture"
  get "/gods", to: "static_pages#gods"
  get "/game_rules", to: "static_pages#game_rules"
  get "/timeline", to: "static_pages#timeline"
  get "/map", to: "static_pages#map"
  get "/asgard", to: "static_pages#asgard"
  get "/crew", to: "static_pages#crew"
  get "/homerules", to: "static_pages#homerules"
  root "static_pages#home"
end
