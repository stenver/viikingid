class StaticPagesController < ApplicationController
  def home
  end

  def culture
  end

  def gods
  end

  def game_rules
  end

  def timeline
  end

  def map
  end

  def crew
  end

  def homerules
  end

  def asgard
  end

  def test
  end
end
